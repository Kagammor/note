var clc = require('cli-color');
var moment = require('moment');

module.exports = function note(source, level) {
    message = Array.prototype.slice.call(arguments, 2);

    if(level.toString().indexOf('!') > -1) {
        message = clc.cyanBright.bold(message);
    }

    var levels = ['INFO', 'WARN', 'ERRO'];
    level = parseInt(level);

    if(level === 1) {
        message = clc.yellowBright(message);
        level = clc.yellowBright('[' + levels[level] + ']');
    } else if(level === 2) {
        message = clc.redBright(message);
        level = clc.redBright('[' + levels[level] + ']');
    } else {
        level = clc.cyanBright('[' + levels[level] + ']');
    }

    while(source.length < 6) {
        source = '_' + source;
    }

    console.log(clc.green('[' + moment().format("YYYY-MM-DD HH:mm:ss") + ']') + clc.cyan('[' + source.substring(0,6).toUpperCase() + ']') + level + ' ' + message);
}
