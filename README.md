# Note
Note is a very minimal error logging prettifier. It adds highlighting and a timestamp.

### Installation
```sh
npm install note-log --save
```

### Syntax
```javascript
note(source, [!]debugLevel, obj1 [, obj2, ..., objN]);
```

#### Source
The source is an arbitrary handle to distinguish (possibly similar) messages from different sources.

```
[2015-01-01 00:00:00][SERVER][INFO] Listening at port 3400
[2015-01-01 00:00:00][___API][INFO] Listening at port 3444
```
The source must be a string and will be formatted to a 6-letter uppercase handle.

#### Debug levels
0. Info
1. Warning
2. Error
* Adding an exclamation mark to the debug level, `#!`, will add additional highlighting.

### Examples
```javascript
note('server', 0, 'Connection established');
// [2015-01-01 00:00:00][SERVER][INFO] Connection established

note('server', 1, 'Disconnected');
// [2015-01-01 00:00:00][SERVER][WARN] Disconnected

fs.readFile('./README.md', function(error, data) {
    if(error) {
        note('server', 2, error);
        // [2015-01-01 00:00:00][SERVER][ERRO] Error: ENOENT, open './README.md'
    } else {
        note('server', 0, data);
        // [2015-01-01 00:00:00][SERVER][INFO] Lorem ipsum dolor sit amet
    }
});
```

#### Notes
Note is still in development. As such, it's missing a few features:
* Note will not expand the data. You may want to use *util.inspect*, or pass e.g. *error.stack* explicitly.
